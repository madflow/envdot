const path = require('path');
const fs = require('fs');
const { parse } = require('dotenv');
const debug = (process.env.DEBUG = '*' ? true : false);
const dotEnvDist = path.join('.env.dist');

if (false === fs.existsSync(dotEnvDist)) {
  console.info(`${dotEnvDist} does not exist.`);
  process.exit(0);
}

if (true === fs.existsSync('.env')) {
  console.error(
    `There is a .env file already in the current working directory. Please remove it first.`
  );
  process.exit(1);
}

const fileContent = fs.readFileSync(dotEnvDist);
const parsed = parse(fileContent);

const output = {};
Object.keys(parsed).forEach(function(key) {
  const processEnvValue = process.env.hasOwnProperty(key)
    ? process.env[key]
    : null;
  const distValue = parsed[key];

  if (processEnvValue && distValue === '') {
    output[key] = processEnvValue;
  } else if (distValue !== '') {
    output[key] = distValue;
  }
});

if(Object.keys(output).length === 0) {
    console.log('No data written');
    process.exit(0);
}

fs.writeFileSync(
  path.join('.env'),
  Object.keys(output)
    .map(key => {
      return `${key}=${output[key]}`;
    })
    .join('\n')
);
