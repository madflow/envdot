# envdot

Generate a `.env` file using the environment variables defined in `.env.dist` as a template. 

If there already is a value defined in the `.env.dist` file it will be left untouched.

```INI
# .env.dist
AAAAH_YEAHHHH=OK
MMMMKAY=
```

```BASH
$ export AAAAH_YEAHHHH=MUKKKAAAAA
$ export MMMMKAY=AAA
$ envdot
```

```INI
# .env
AAAAH_YEAHHHH=OK
MMMMKAY=AAA
```


